package cest.edu.br.segundaprova;

public interface Atendimento {
	public String getSigla();
	public String getDescricao();

	public String getNome();
	
	public String getCidade();
	public String getLogradouro();
}
